﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OrbiServer.Entities;

namespace OrbiServer.Controllers
{
    public class GoalController
    {
       [HttpPost("goal/add")]
        public IActionResult AddGoal(string type,int author,string startTime, string deadLine)
        {
            if (string.IsNullOrEmpty(type) || author == 0 || string.IsNullOrEmpty(startTime) || string.IsNullOrEmpty(deadLine))
            {
                return ResponseController.CreatedResponse("Bad Request", StatusCodes.Status400BadRequest);
            }

            try
            { 
                using (UsersContext dataContext = new UsersContext())
                {
                    Target goal = new Target()
                    {
                        Type = type,
                        UserId = author,
                        StartTime = startTime,
                        DeadLine = deadLine
                    };

                    dataContext.Targets.Add(goal);
                    dataContext.SaveChanges();
                    return new JsonResult(goal);
                   
                }
            }
            catch (Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message, StatusCodes.Status403Forbidden);
            }
        }

        [HttpGet("goal")]
        public IActionResult GetGoal(int id)
        {
            try
            {
                using (UsersContext dataContext = new UsersContext())
                {
                    Target goal = dataContext.Targets.Where(g => g.Id == id).FirstOrDefault();

                    if (goal == null)
                    {
                        return ResponseController.CreatedResponse("Don't found goal", StatusCodes.Status403Forbidden);
                    }

                    return ResponseController.CreatedResponse(JsonConvert.SerializeObject(goal), StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message, StatusCodes.Status403Forbidden);
            }
        }

        [HttpGet("goal/del")]
        public IActionResult DeleteGoal(int id)
        {
            try
            {
                using (UsersContext dataContext = new UsersContext())
                {
                    Target goal = dataContext.Targets.Where(g => g.Id == id).FirstOrDefault();

                    if (goal == null)
                    {
                        return ResponseController.CreatedResponse("Don't found goal", StatusCodes.Status403Forbidden);
                    }

                    dataContext.Targets.Remove(goal);
                    dataContext.SaveChanges();

                    return ResponseController.CreatedResponse("Success delete goal", StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message, StatusCodes.Status403Forbidden);
            }
        }

        [HttpGet("goal/byTime")]
        public IActionResult GetAllGoalsByStartTime(string startTime)
        {
            try
            {
                using (UsersContext dataContext = new UsersContext())
                {
                    List<Target> goals = dataContext.Targets.Where(g => DateTime.Parse(g.StartTime).Day == DateTime.Parse(startTime).Day).ToList();

                    if (goals == null)
                    {
                        return ResponseController.CreatedResponse("Don't get goals", StatusCodes.Status403Forbidden);
                    }

                    return ResponseController.CreatedResponse(JsonConvert.SerializeObject(goals), StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message, StatusCodes.Status403Forbidden);
            }
        }


        [HttpGet("goal/all")]
        public IActionResult GetAllGoals()
        {
            try
            {
                using (UsersContext dataContext = new UsersContext())
                {
                    List<Target> goals = dataContext.Targets.ToList();

                    if (goals == null)
                    {
                        return ResponseController.CreatedResponse("Don't get goals", StatusCodes.Status403Forbidden);
                    }

                    return ResponseController.CreatedResponse(JsonConvert.SerializeObject(goals), StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message, StatusCodes.Status403Forbidden);
            }
        }


 
    }
}
