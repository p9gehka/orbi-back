﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using OrbiServer.Entities;
using Microsoft.AspNetCore.Http;

namespace OrbiServer.Controllers
{
    
    public class PeopleController
    {
        [HttpPost("signup")]
        public IActionResult AddUser(string login,string password)
        {
            bool errorValidate = false;
    
            if (string.IsNullOrEmpty(login))
                errorValidate = true;

            if (string.IsNullOrEmpty(password))
                errorValidate = true;

       

            if (errorValidate)
            {
                return ResponseController.CreatedResponse("No validate login or password",StatusCodes.Status403Forbidden);
            }
            

            string securePassword = CryptoController.GenerateHash(password);

            try
            {
                using (UsersContext context = new UsersContext())
                {

                    User user = context.Users.Where(u => u.Login == login).FirstOrDefault();

                    if (user ==  null)
                    {
                        user = new User
                        {
                            Login = login,
                            Password = securePassword
                        };
                        context.Users.Add(user);

                        context.SaveChanges();
                        
                    }

                    return new JsonResult(user);


                }
            }
            catch(Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message,StatusCodes.Status403Forbidden);
            }
        }


        [HttpGet("user/get")]
        public IActionResult GetUser(int id = 1)
        {
            try
            {
                using (UsersContext context = new UsersContext())
                {
                    User user = context.Users.First(u => u.Id == id);


                    if(user == null)
                    {
                        return ResponseController.CreatedResponse("Not found user", StatusCodes.Status403Forbidden);
                    }

                    return ResponseController.CreatedResponse($"Success found User -> {user.Login}", StatusCodes.Status200OK);
                }
            }
            catch(Exception ex)
            {
                return ResponseController.CreatedResponse(ex.Message, StatusCodes.Status403Forbidden);
            }
        }
    }

    


}
