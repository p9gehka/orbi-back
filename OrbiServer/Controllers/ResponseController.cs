﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace OrbiServer.Controllers
{
    internal static class ResponseController
    {
        public static IActionResult CreatedResponse(object value, int? statusCode)
        {
            return new ObjectResult(value)
            {
                Value = value,
                StatusCode = statusCode
            };
        }
    }
}
