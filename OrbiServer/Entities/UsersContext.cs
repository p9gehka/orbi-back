﻿using System;
using Microsoft.EntityFrameworkCore;
using OrbiServer.Entities;

public class UsersContext : DbContext
{

    static readonly string connectString = "Host=127.0.0.1;Port=5432;Database=orbi_db;Username=orbi;Password=orbi";

    public UsersContext()
    {
        DbContextOptionsBuilder dbContextOptionsBuilder = new DbContextOptionsBuilder();
        OnConfiguring(dbContextOptionsBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(connectString);
    }

    public DbSet<Transaction> Transactions { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Target> Targets { get; set; }
}
