﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace OrbiServer.Entities
{
    [DataContract]
    public class User
    {
        [Key]
        [DataMember]
        public int Id { get; set; }
        [StringLength(50)]
        [DataMember]
        public string Login { get; set; }

        public List<Target> Targets { get; set; }

        [StringLength(50)]
        public string Password { get; set; }
    }
}
