﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrbiServer.Entities
{
    [Serializable]
    public class Target
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Type { get; set; }
        [Obsolete]
        public int Author { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        [StringLength(50)]
        public string StartTime { get; set; }
        [StringLength(50)]
        public string DeadLine { get; set; }
        public int Balance { get; set; }
    }
}
