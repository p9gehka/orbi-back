﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OrbiServer.Entities
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }
        public int Sender { get; set; }
        public int Received { get; set; }
        public int Amount { get; set; }
        public string OperationTime { get; set; }
        public int Goal { get; set; }

    }
}
